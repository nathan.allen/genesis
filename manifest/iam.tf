# https://www.terraform.io/docs/providers/aws/r/iam_user.html

resource "aws_iam_user" "kops" {
  name = "kops"
}

resource "aws_iam_access_key" "kops" {
  user = "${aws_iam_user.kops.name}"
}

# https://www.terraform.io/docs/providers/aws/r/iam_user_policy_attachment.html

resource "aws_iam_user_policy_attachment" "p1" {
  user       = "${aws_iam_user.kops.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

resource "aws_iam_user_policy_attachment" "p2" {
  user       = "${aws_iam_user.kops.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess"
}

resource "aws_iam_user_policy_attachment" "p3" {
  user       = "${aws_iam_user.kops.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonRoute53FullAccess"
}

resource "aws_iam_user_policy_attachment" "p4" {
  user       = "${aws_iam_user.kops.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_user_policy_attachment" "p5" {
  user       = "${aws_iam_user.kops.name}"
  policy_arn = "arn:aws:iam::aws:policy/IAMFullAccess"
}

resource "aws_iam_user_policy_attachment" "p6" {
  user       = "${aws_iam_user.kops.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonVPCFullAccess"
}

resource "aws_iam_user" "ses" {
  name = "ses"
}

resource "aws_iam_access_key" "ses" {
  user = "${aws_iam_user.ses.name}"
}

resource "aws_iam_user_policy_attachment" "ses" {
  user       = "${aws_iam_user.ses.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSESFullAccess"
}

resource "aws_iam_user" "sqs" {
  name = "sqs"
}

resource "aws_iam_access_key" "sqs" {
  user = "${aws_iam_user.sqs.name}"
}

resource "aws_iam_user_policy_attachment" "sqs" {
  user       = "${aws_iam_user.sqs.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}
