# Note - no backup by default

resource "aws_security_group" "allow_psql" {
  name        = "allow_psql"
  description = "Allow inbound Postgres traffic"
  vpc_id      = "${var.vpc_default_id}"

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "gnar" {
  allocated_storage      = 20
  storage_type           = "gp2"

  publicly_accessible    = true
  vpc_security_group_ids = ["${aws_security_group.allow_psql.id}"]

  engine                 = "postgres"
  engine_version         = "10.4"

  instance_class         = "db.t2.micro"
  parameter_group_name   = "default.postgres10"

  identifier             = "${var.db_identifier}"

  username               = "${var.db_username}"
  password               = "${var.db_password}"

  skip_final_snapshot    = true
}
