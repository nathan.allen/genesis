import random
import subprocess
from json import loads

bashCommand = "aws ec2 describe-subnets"
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
subnets, error = process.communicate()

subnets = loads(subnets.decode())

avail_blocks = list(range(16))

for subnet in subnets['Subnets']:
    block = int(subnet["CidrBlock"].split('.')[2]) / 16
    avail_blocks.remove(block)

if len(avail_blocks) < 3:
    raise ValueError('No available CIDR blocks.')

subnet_blocks = avail_blocks[:3]
cidr_prefix = '.'.join(subnets['Subnets'][0]['CidrBlock'].split('.')[:2])

picked_subnets = ['{}.{}.0/20'.format(cidr_prefix, block * 16) for block in subnet_blocks]

for idx, subnet in enumerate(picked_subnets):
    print("export TF_VAR_subnet_{}='{}'".format(chr(97 + idx), subnet))

random_default_subnet = random.choice([subnet for subnet in subnets['Subnets'] if subnet['DefaultForAz']])

print("export GNAR_KOPS_SUBNET_ZONE='{}'".format(random_default_subnet['AvailabilityZone']))
print("export GNAR_KOPS_SUBNET_ID='{}'".format(random_default_subnet['SubnetId']))
